struct Tarea {
	int ID_Tarea;
	char *Nombre;
	char Estado;
};

void Set_ID_Tarea(struct tarea, int);

void Set_Nombre(struct tarea, char* );

void Set_Estado(struct tarea, char );

int Get_ID_Tarea(struct tarea);

char Get_Nombre(struct tarea);

char Get_Estado(struct tarea);

void Crear_Tarea(struct tarea);

void Ejecutar_Tarea(struct tarea);
